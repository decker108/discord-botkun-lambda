import json
import unittest
from datetime import datetime
from os import environ
from unittest import mock

import psycopg2
from psycopg2.extras import DictCursor

environ['DB_URL'] = '0.0.0.0'
environ['DB_NAME'] = 'botkundb'
environ['DB_USER'] = 'botkun_db_user'
environ['DB_PASSWORD'] = 'secret'
environ['RSS_URL'] = 'http://localhost:1337/rss'
environ['DISCORD_WEBHOOK_ID'] = '123456789'
environ['DISCORD_WEBHOOK_TOKEN'] = '548eb755_9009_4ff4_ba20_fc8a2046f51c'
from packages.botkun.botkun_lambda.__main__ import main


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, body, status_code):
            self.status_code = status_code
            self.ok = True
            self.encoding = 'utf-8'
            self.text = body

    with open('resources/another_example_rss.xml', 'rt', encoding='utf-8') as fd:
        feedText = fd.read()

    if args[0] == 'http://localhost:1337/rss':
        return MockResponse(feedText, 200)

    return MockResponse(None, 404)


def mocked_requests_post(*args, **kwargs):
    class MockResponse:
        def __init__(self, body, status_code):
            self.status_code = status_code
            self.ok = True
            self.encoding = 'utf-8'
            self.text = body

    if args[0] == 'https://discord.com/api/webhooks/123456789/548eb755_9009_4ff4_ba20_fc8a2046f51c?wait=true':
        return MockResponse('OK', 200)

    return MockResponse(None, 404)


class BotkunIntegrationTestCase(unittest.TestCase):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_fullFlow(self, mocked_post, mocked_get):
        expected = {
            'lastseenentryid': '2918cc2f3287755af4a8a1b8f84ec9a80056e660',
            'lastupdatedate': datetime(2022, 7, 2),
            'name': 'test_manga',
            'seriesid': 'khaoov3',
        }
        expectedDiscordMsg = {
            "content": "The following manga have new chapters:",
            "username": "Botkun",
            "embeds": [{
                "title": "Tomb Raider King",
                "url": "https://www.mangaupdates.com/series/khaoov3/tomb-raider-king",
                "description": "[Luminous] Tomb Raider King c.337-338",
                "type": "rich"
            }]
        }
        self.insertTestDataIntoDb(title='test_manga', seriesId='khaoov3')

        main(None)

        result = self.getMangaWithSeriesId('khaoov3')
        actual = {k: result[k] for k in result.keys()}
        self.assertEqual(actual, expected | actual)
        self.assertIn(mock.call('http://localhost:1337/rss'), mocked_get.call_args_list)
        self.assertEqual(mocked_post.call_args_list[0][0][0], 'https://discord.com/api/webhooks/123456789/548eb755_9009_4ff4_ba20_fc8a2046f51c?wait=true')
        self.assertEqual(mocked_post.call_args_list[0][1]['data'], json.dumps(expectedDiscordMsg))

    @staticmethod
    def insertTestDataIntoDb(title, seriesId):
        with psycopg2.connect(dbname='botkundb', user='postgres', password='test', host='localhost') as conn:
            with conn.cursor() as cur:
                cur.execute('TRUNCATE TABLE tracked_manga')
                cur.execute("""
                    INSERT INTO tracked_manga(name, url, seriesid) 
                    VALUES(%s, %s, %s)""", (title, 'https://example.com', seriesId))

    @staticmethod
    def getMangaWithSeriesId(seriesId):
        with psycopg2.connect(dbname='botkundb', user='postgres', password='test', host='localhost', cursor_factory=DictCursor) as conn:
            with conn.cursor() as cur:
                cur.execute('SELECT * FROM tracked_manga WHERE seriesid = %s', (seriesId,))
                return cur.fetchone()


if __name__ == '__main__':
    unittest.main()
