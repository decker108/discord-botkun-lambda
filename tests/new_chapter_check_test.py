import unittest
from packages.botkun.botkun_lambda.__main__ import checkForNewChapters
from unittest import mock
from datetime import datetime


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, body, status_code):
            self.status_code = status_code
            self.ok = True
            self.encoding = 'utf-8'
            self.text = body

    with open('resources/another_example_rss.xml', 'rt', encoding='utf-8') as fd:
        feedText = fd.read()

    if args[0] == 'http://localhost:1337/rss':
        return MockResponse(feedText, 200)

    return MockResponse(None, 404)


class NewChapterCheckTestCase(unittest.TestCase):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_checkForNewChapters_shouldHandleSingleNewChapter(self, mock_get):
        expected = {
            'entryId': 'd8906a69be05a14f44b0a9b5529c2f3df0ca02ea',
            'link': 'https://www.mangaupdates.com/series/bj0un1d/volcanic-age',
            'rawTitle': '[Luminous] Volcanic Age c.211',
            'seriesId': 'bj0un1d',
            'title': 'Volcanic Age',
            'updateDate': datetime(2022, 7, 2).date()
        }
        mockRssUrl = 'http://localhost:1337/rss'
        exampleTrackedManga = [
            {
                'seriesId': 'bj0un1d',
                'lastSeenEntryId': 'someid',
                'lastUpdateDate': datetime(2022, 6, 1),
            }
        ]
        actual = checkForNewChapters(exampleTrackedManga, mockRssUrl)
        self.assertEqual(len(actual), 1)
        self.assertEqual(actual[0], expected | actual[0])

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_checkForNewChapters_shouldHandleMultipleNewChapters(self, mock_get):
        expected = {
            'entryId': '2918cc2f3287755af4a8a1b8f84ec9a80056e660',
            'link': 'https://www.mangaupdates.com/series/khaoov3/tomb-raider-king',
            'rawTitle': '[Luminous] Tomb Raider King c.337-338',
            'seriesId': 'khaoov3',
            'title': 'Tomb Raider King',
            'updateDate': datetime(2022, 7, 2).date()
        }
        mockRssUrl = 'http://localhost:1337/rss'
        exampleTrackedManga = [
            {
                'seriesId': 'khaoov3',
                'lastSeenEntryId': 'anotherid',
                'lastUpdateDate': datetime(2022, 5, 1),
            }
        ]
        actual = checkForNewChapters(exampleTrackedManga, mockRssUrl)
        self.assertEqual(len(actual), 1)
        self.assertEqual(actual[0], expected | actual[0])


if __name__ == '__main__':
    unittest.main()
