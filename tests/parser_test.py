import unittest
from datetime import date
from packages.botkun.botkun_lambda.__main__ import parseFeed, prettifyEntryTitle, parseUpdateDate, extractSeriesId

class ParserTestCases(unittest.TestCase):

    def test_parseFeed(self):
        with open('resources/example_rss.xml', 'rt') as fd:
            feedText = fd.read()
        result = parseFeed(feedText)
        self.assertEqual(98, len(result))

    def test_parseUpdateDate(self):
        exampleStr = """2022-07-01&lt;br /&gt;&lt;a href="https://www.mangaupdates.com/series.html?id=44313681582"&gt;Boku to Majo ni Tsuite no Bibouroku&lt;/a&gt; - &lt;a href="https://www.mangaupdates.com/releases.html?search=44313681582&amp;stype=series"&gt;View All Releases&lt;/a&gt;"""
        result = parseUpdateDate(exampleStr)
        self.assertEqual(date.fromisoformat("2022-07-01"), result)

    def test_prettifyEntryTitle_stringWithGroupAndChapter(self):
        exampleStr = '[Yuzukawa scans] Yuzukawa-san wa, Sasshite Hoshii. c.30-30.5'
        result = prettifyEntryTitle(exampleStr)
        self.assertEqual('Yuzukawa-san wa, Sasshite Hoshii.', result)

    def test_prettifyEntryTitle_stringWithGroupAndVolumeAndChapter(self):
        exampleStr = '[Kinjo Scans] Shokuryou Jinrui Re: Starving Re:velation v.3 c.16'
        result = prettifyEntryTitle(exampleStr)
        self.assertEqual('Shokuryou Jinrui Re: Starving Re:velation', result)

    def test_extractSeriesId(self):
        exampleStr = 'https://www.mangaupdates.com/series/khaoov3/tomb-raider-king'
        result = extractSeriesId(exampleStr)
        self.assertEqual('khaoov3', result)


if __name__ == '__main__':
    unittest.main()
