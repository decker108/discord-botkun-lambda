# Digital Ocean lambdas

A repository for experimenting with DO lambdas.

## Deployment

Install the `doctl` program, cd to the folder of the lambda and run the following to deploy:

    doctl sandbox deploy . --remote-build

To use a different set of environment variables for templating, run this:

    doctl sandbox deploy --env production.env . --remote-build

## Usage

Run this:

    doctl sandbox functions invoke botkun/botkun_lambda

Or using http:

    curl https://faas-ams3-2a2df116.doserverless.co/api/v1/web/<ID>/botkun/botkun_lambda