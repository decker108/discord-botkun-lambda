ALTER TABLE tracked_manga RENAME COLUMN entryid TO seriesid;

ALTER TABLE tracked_manga ADD COLUMN lastseenentryid varchar(41);
