CREATE TABLE metadata(last_update_date TIMESTAMP DEFAULT NOW());

INSERT INTO metadata(last_update_date) VALUES(NOW());
GRANT SELECT,INSERT,UPDATE,DELETE ON metadata TO botkun_db_user;