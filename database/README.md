# Database

This project uses a Supabase psql database. 
To initialize the schema, run Flyway with the files in the `schema` folder.

## Accessing

    psql -h db.<SECRET_ID>.supabase.co -p 5432 -d postgres -U postgres

## Running migrations

The migrations cannot create the database, so make sure you create the db 
manually first by logging into the db and running `create database botkundb;`. 

Then execute the below command in the folder of this readme to run the migrations:

    flyway -url="jdbc:postgresql://db.<SECRET_ID>.supabase.co:5432/botkundb?user=postgres&password=<DB_PASSWORD>" migrate
