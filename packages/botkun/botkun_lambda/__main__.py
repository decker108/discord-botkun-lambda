import datetime
import hashlib
import json
import re
import traceback
from datetime import date
from os import environ

import feedparser
import psycopg2
import requests
from psycopg2.extras import DictCursor


def getMessage(manga):
    return json.dumps({
        "content": "The following manga have new chapters:",
        "username": "Botkun",
        "embeds": [{
            "title": manga['title'],
            "url": manga['link'],
            "description": manga['rawTitle'] or '',
            "type": "rich"
        }]
    })


def sendDiscordMsg(manga, webhookId, webhookToken):
    requestBody = getMessage(manga)
    headers = {"Content-Type": "application/json"}
    r = requests.post(f'https://discord.com/api/webhooks/{webhookId}/{webhookToken}?wait=true',
                      data=requestBody, headers=headers)
    if not r.ok:
        raise RuntimeError(f"Failed to send discord msg: {r.status_code} {r.text}")


def replaceAfterLast(inputStr, subStr):
    idx = inputStr.rfind(subStr)
    if idx > -1:
        return inputStr.replace(inputStr[idx+len(subStr):], '')
    return inputStr


def prettifyEntryTitle(rawTitle):
    # noinspection RegExpRedundantEscape
    cleanedTitle = re.sub('\[.*\]\s', '', rawTitle)
    cleanedTitle = replaceAfterLast(cleanedTitle, ' v.')
    cleanedTitle = replaceAfterLast(cleanedTitle, ' c.')
    cleanedTitle = cleanedTitle.replace(' v.', '')
    cleanedTitle = cleanedTitle.replace(' c.', '')
    return cleanedTitle


def parseUpdateDate(summary):
    maybeDate = summary[0:10]
    if re.match('\d{4}-\d{2}-\d{2}', maybeDate):
        updateDate = date.fromisoformat(maybeDate)
        return updateDate or date.today()
    return date.today()


def extractSeriesId(link):
    return link.split('/')[4]


def hashString(inputStr):
    return hashlib.sha1(str.encode(inputStr)).hexdigest()


def parseFeed(feedTextOrFile):
    parsedFeed = feedparser.parse(feedTextOrFile)
    if parsedFeed.bozo == 1:
        raise IOError("RSS feed is malformed")
    entries = []
    for entry in parsedFeed.entries:
        if 'link' not in entry:
            continue
        if '[Series Data Missing or Unavailable]' in entry['summary']:
            continue
        entries += [{
            "entryId": hashString(entry["title"]),
            "seriesId": extractSeriesId(entry["link"]),
            "title": prettifyEntryTitle(entry["title"]),
            "rawTitle": entry["title"],
            "summary": entry["summary"],
            "link": entry["link"],
            "updateDate": parseUpdateDate(entry['summary'])
        }]
    return entries


def checkForNewChapters(trackedMangaList, rssUrl):
    results = []
    resp = requests.get(rssUrl)
    if not resp.ok:
        raise RuntimeError(f'Failed to get RSS: {resp.status_code} {resp.text}')
    entries = parseFeed(resp.text)
    if len(entries) > 0:
        for manga in trackedMangaList:
            matches = [el for el in entries if el['seriesId'] == manga['seriesid']]
            if len(matches) == 1:
                foundManga = matches[0]
                if foundManga['entryId'] != manga['lastseenentryid']:
                    results += [foundManga]
            elif len(matches) > 1:
                newMatches = [el for el in matches if el['entryId'] != manga['lastseenentryid']]
                if len(newMatches) > 0:
                    foundManga = max(newMatches, key=lambda el: el['updateDate'])
                    if foundManga['updateDate'] > manga['lastupdatedate']:
                        results += [foundManga]
    return results


def updateMangaWithLatestRelease(cur, manga):
    cur.execute("""
        UPDATE tracked_manga
        SET lastupdatedate = %s,
            lastseenentryid = %s
        WHERE id = (SELECT id FROM tracked_manga WHERE seriesid = %s)
    """, (manga['updateDate'], manga['entryId'], manga['seriesId']))


def markFeedsChecked(cur, checkDate):
    cur.execute("""
        UPDATE metadata
        SET last_update_date = %s
    """, (checkDate,))


def main(args):
    # name = args.get("name", "stranger")
    # print("Env vars:", [(k,v) for k,v in environ.items()])
    print(environ.get('MY_TEST_ENV_VAR', 'missing'))  # works!
    # print(dir(psycopg2))
    dbUrl = environ['DB_URL']
    dbName = environ['DB_NAME']
    dbUser = environ['DB_USER']
    dbPassword = environ['DB_PASSWORD']
    rssUrl = environ['RSS_URL']
    webhookId = environ['DISCORD_WEBHOOK_ID']
    webhookToken = environ['DISCORD_WEBHOOK_TOKEN']

    try:
        with psycopg2.connect(dbname=dbName, user=dbUser, password=dbPassword, host=dbUrl, cursor_factory=DictCursor) as conn:
            with conn.cursor() as cur:
                cur.execute('SELECT CAST(lastupdatedate AS DATE), seriesid, lastseenentryid FROM tracked_manga')
                trackedMangaList = cur.fetchall()

                updatedManga = checkForNewChapters(trackedMangaList, rssUrl)
                for manga in updatedManga:
                    sendDiscordMsg(manga, webhookId, webhookToken)
                    updateMangaWithLatestRelease(cur, manga)
                markFeedsChecked(cur, datetime.datetime.now())
    except Exception as e:
        exctype = type(e)
        tb = e.__traceback__
        print('Error executing lambda')
        traceback.print_exception(exctype, e, tb)
        return {'result': 'error'}

    return {'result': 'success'}
